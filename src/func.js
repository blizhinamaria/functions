const getSum = (str1, str2) => {
  if (typeof(str1) !== "string" || typeof(str2) !== "string" || isNaN(str1) || isNaN(str2))
    return false;
  let num1 = 0; 
  let num2 = 0;
  num1 = str1.length === 0 ? 0 : parseInt(str1);
  num2 = str2.length === 0 ? 0 : parseInt(str2);
  return "" + (num1 + num2);

};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let postsNumber = 0;
  let commentsNumber = 0;
  for (let post of listOfPosts){
    if (post.author === authorName)
      postsNumber++;
    let comments = post.comments;
    if (post.comments){
      for (let i=0; i<comments.length; i++){
        if (comments[i].author === authorName)
          commentsNumber++;
      }
    }

  }
  return "Post:" + postsNumber + ",comments:" + commentsNumber;
};

const tickets=(people)=> {
  let amounts = people.map(c => parseInt(c));
  let rest = 0;
  for (let amount of amounts){
    if ((amount - 25) > rest)
      return 'NO';
    else
     rest += 25;
  }
  return 'YES';
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
